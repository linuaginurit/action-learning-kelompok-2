-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2022 at 08:17 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineexam`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `exam` varchar(25) NOT NULL,
  `subname` varchar(25) NOT NULL,
  `subcode` varchar(25) NOT NULL,
  `registernum` varchar(25) NOT NULL,
  `quenum` int(10) NOT NULL,
  `answer` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`exam`, `subname`, `subcode`, `registernum`, `quenum`, `answer`) VALUES
('First Exam', 'java', 'java-123', 'Rn1000', 10, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 9, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 8, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 7, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 6, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 5, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 4, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 3, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 2, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 1, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 1, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 2, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 3, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 4, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 5, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 6, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 7, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 8, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 9, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 10, 3),
('Exam Name', 'null', 'null', 'Rn1000', 1, 0),
('Exam Name', 'null', 'null', 'Rn1000', 1, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 2, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 3, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 4, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 5, 2),
('First Exam', 'java', 'java-123', 'Rn1000', 6, 2),
('Exam Name', 'null', 'null', 'Rn1000', 1, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 2, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 3, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 4, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 5, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 6, 2),
('First Exam', 'Math', 'math-001', 'Rn1002', 1, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 2, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 1, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 2, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 3, 1),
('Exam Name', 'null', 'null', 'Rn1000', 1, 1),
('First Exam', 'java', 'java-123', 'Rn1000', 2, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 3, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 4, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 5, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 6, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 7, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 8, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 9, 3),
('First Exam', 'java', 'java-123', 'Rn1000', 10, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 1, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 2, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 3, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 4, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 5, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 6, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 7, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 8, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 9, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 10, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 11, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 12, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 13, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 14, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 15, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 16, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 17, 3),
('First Exam', 'Math', 'math-001', 'Rn1002', 18, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 19, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 20, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 21, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 22, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 23, 1),
('First Exam', 'Math', 'math-001', 'Rn1002', 24, 1),
('Exam Name', 'null', 'null', 'Rn1000', 1, 1),
('Exam Name', 'null', 'null', 'Rn1000', 2, 3),
('Exam Name', 'null', 'null', 'Rn1000', 3, 4),
('Exam Name', 'null', 'null', 'Rn1000', 4, 2),
('Exam Name', 'null', 'null', 'Rn1000', 5, 3),
('Exam Name', 'null', 'null', 'Rn1000', 6, 4),
('Exam Name', 'null', 'null', 'Rn1000', 7, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 8, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 9, 4),
('First Exam', 'java', 'java-123', 'Rn1000', 10, 4),
('Exam Name', 'null', 'null', 'Rn1000', 1, 1),
('Exam Name', 'null', 'null', 'Rn1000', 2, 1),
('Exam Name', 'null', 'null', 'Rn1000', 3, 1),
('Exam Name', 'null', 'null', 'Rn1000', 4, 1),
('Exam Name', 'null', 'null', 'Rn1000', 5, 1),
('First Exam', 'Umum', 'umum', 'Rn1002', 1, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 2, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 3, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 4, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 5, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 1, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 2, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 3, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 4, 4),
('First Exam', 'Umum', 'umum', 'Rn1002', 5, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 6, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 7, 3),
('First Exam', 'Umum', 'umum', 'Rn1002', 8, 1),
('First Exam', 'Umum', 'umum', 'Rn1002', 9, 2),
('First Exam', 'Umum', 'umum', 'Rn1002', 10, 4);

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE `mark` (
  `subcode` varchar(25) NOT NULL,
  `regnum` varchar(25) NOT NULL,
  `totalque` int(25) NOT NULL,
  `correctans` int(25) NOT NULL,
  `falseans` int(25) NOT NULL,
  `avgmark` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mark`
--

INSERT INTO `mark` (`subcode`, `regnum`, `totalque`, `correctans`, `falseans`, `avgmark`) VALUES
('java-123', 'Rn1000', 10, 0, 10, '0.0%'),
('java-123', 'Rn1000', 10, 1, 9, '10.0%'),
('umum', 'Rn1002', 5, 5, 0, '50.0%'),
('umum', 'Rn1002', 10, 10, 0, '100.0%');

-- --------------------------------------------------------

--
-- Table structure for table `setpaper`
--

CREATE TABLE `setpaper` (
  `exam` varchar(50) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `subcode` varchar(20) NOT NULL,
  `grade` varchar(20) NOT NULL,
  `queNumber` int(25) NOT NULL,
  `que` varchar(250) NOT NULL,
  `ans01` varchar(100) NOT NULL,
  `ans02` varchar(100) NOT NULL,
  `ans03` varchar(100) NOT NULL,
  `ans04` varchar(100) NOT NULL,
  `correctans` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setpaper`
--

INSERT INTO `setpaper` (`exam`, `subject`, `subcode`, `grade`, `queNumber`, `que`, `ans01`, `ans02`, `ans03`, `ans04`, `correctans`) VALUES
('First Exam', 'Umum', 'umum', 'Grade 01', 9, '5+6', '12', '11', '10', '15', '2'),
('First Exam', 'Umum', 'umum', 'Grade 01', 10, '10+5', '12', '11', '10', '15', '4'),
('First Exam', 'Umum', 'umum', 'Grade 01', 4, 'Apakah kepanjangan PBB?', 'Perkumpulan Bapak Bapak', 'Persatuan Bangsa Bangsa', 'Perkumpulan Bangsa Bangsa', 'Perserikatan Bangsa Bangsa', '4'),
('First Exam', 'Umum', 'umum', 'Grade 01', 5, 'Jenis makanan berikut yang tidak termasuk kabohidrat adalah', 'nasi', 'jagung', 'telur', 'ketan', '3'),
('First Exam', 'Umum', 'umum', 'Grade 01', 1, 'Kerajaan hindu tertua di Indonesia adalah kerajaan ?', 'Majapahit', 'Mataram Lama', 'singasari', 'Kutai', '4'),
('First Exam', 'Umum', 'umum', 'Grade 01', 2, 'Teks Proklamasi yang telah disetujui diketik oleh?', 'Tri Sutrisno ', 'M. Hatta', 'Sayuti Melik', 'Wr Supratman', '3'),
('First Exam', 'Umum', 'umum', 'Grade 01', 3, 'Pertempuran Margarana di Bali dipimpin oleh?', 'Letkol M. Sarbini', 'M. Hatta', 'I Gusti ketut jelantik', 'Letkol I Gusti Ngurah Rai', '4'),
('First Exam', 'Umum', 'umum', 'Grade 01', 6, 'Lingkungan Tempat tinggal makhluk hidup dise ut', 'ekosistem', 'populasi', 'habitat', 'adaptasi', '3'),
('First Exam', 'Umum', 'umum', 'Grade 01', 7, '5+5', '12', '11', '10', '15', '3'),
('First Exam', 'Umum', 'umum', 'Grade 01', 8, '5+7', '12', '11', '10', '15', '1');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `rNumber` varchar(25) NOT NULL,
  `sName` varchar(50) NOT NULL,
  `sbirthday` varchar(25) NOT NULL,
  `grade` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`rNumber`, `sName`, `sbirthday`, `grade`, `password`) VALUES
('Rn1000', 'jhiu', '2019-09-05', 'Grade 02', '123'),
('Rn1001', 'klmkl', '2019-09-12', 'Grade 03', '12'),
('Rn1002', 'Agung F', '1999-05-25', 'Grade 01', '123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
