/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import java.util.Objects;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class Answer {
    
    String exam, subname, subcode, registernum;
    int quenum, answer;

    public Answer() {
    }

    public Answer(String exam, String subname, String subcode, String registernum, int quenum, int answer) {
        this.exam = exam;
        this.subname = subname;
        this.subcode = subcode;
        this.registernum = registernum;
        this.quenum = quenum;
        this.answer = answer;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getRegisternum() {
        return registernum;
    }

    public void setRegisternum(String registernum) {
        this.registernum = registernum;
    }

    public int getQuenum() {
        return quenum;
    }

    public void setQuenum(int quenum) {
        this.quenum = quenum;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "Answer{" + "exam=" + exam + ", subname=" + subname + ", subcode=" + subcode + ", registernum=" + registernum + ", quenum=" + quenum + ", answer=" + answer + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.exam);
        hash = 67 * hash + Objects.hashCode(this.subname);
        hash = 67 * hash + Objects.hashCode(this.subcode);
        hash = 67 * hash + Objects.hashCode(this.registernum);
        hash = 67 * hash + this.quenum;
        hash = 67 * hash + this.answer;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Answer other = (Answer) obj;
        if (this.quenum != other.quenum) {
            return false;
        }
        if (this.answer != other.answer) {
            return false;
        }
        if (!Objects.equals(this.exam, other.exam)) {
            return false;
        }
        if (!Objects.equals(this.subname, other.subname)) {
            return false;
        }
        if (!Objects.equals(this.subcode, other.subcode)) {
            return false;
        }
        return Objects.equals(this.registernum, other.registernum);
    }
    
    
    
}
