/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import java.util.Objects;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class Mark {
    String subcode, regnum, avgmark;
    int totalque, correctans, falseans;

    public Mark() {
    }

    public Mark(String subcode, String regnum, String avgmark, int totalque, int correctans, int falseans) {
        this.subcode = subcode;
        this.regnum = regnum;
        this.avgmark = avgmark;
        this.totalque = totalque;
        this.correctans = correctans;
        this.falseans = falseans;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getRegnum() {
        return regnum;
    }

    public void setRegnum(String regnum) {
        this.regnum = regnum;
    }

    public String getAvgmark() {
        return avgmark;
    }

    public void setAvgmark(String avgmark) {
        this.avgmark = avgmark;
    }

    public int getTotalque() {
        return totalque;
    }

    public void setTotalque(int totalque) {
        this.totalque = totalque;
    }

    public int getCorrectans() {
        return correctans;
    }

    public void setCorrectans(int correctans) {
        this.correctans = correctans;
    }

    public int getFalseans() {
        return falseans;
    }

    public void setFalseans(int falseans) {
        this.falseans = falseans;
    }

    @Override
    public String toString() {
        return "Mark{" + "subcode=" + subcode + ", regnum=" + regnum + ", avgmark=" + avgmark + ", totalque=" + totalque + ", correctans=" + correctans + ", falseans=" + falseans + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.subcode);
        hash = 53 * hash + Objects.hashCode(this.regnum);
        hash = 53 * hash + Objects.hashCode(this.avgmark);
        hash = 53 * hash + this.totalque;
        hash = 53 * hash + this.correctans;
        hash = 53 * hash + this.falseans;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mark other = (Mark) obj;
        if (this.totalque != other.totalque) {
            return false;
        }
        if (this.correctans != other.correctans) {
            return false;
        }
        if (this.falseans != other.falseans) {
            return false;
        }
        if (!Objects.equals(this.subcode, other.subcode)) {
            return false;
        }
        if (!Objects.equals(this.regnum, other.regnum)) {
            return false;
        }
        return Objects.equals(this.avgmark, other.avgmark);
    }
    
    
}
