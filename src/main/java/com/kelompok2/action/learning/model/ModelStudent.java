/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import com.kelompok2.action.learning.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class ModelStudent {
    ArrayList<Student> data;
    ArrayList<String> autoId;
    private String tablename = "student";
    
    public Student auth(String username,String password) throws SQLException{
        String query = "SELECT rNumber,grade, password FROM student WHERE rNumber=? and password=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, username);
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            return new Student(rs.getString("rNumber"),rs.getString("grade"),rs.getString("password"));
        }
        return null;
    }

    
    public void setData(ArrayList<Student> data) {
        this.data = data;
    }

    public ModelStudent() {
        data = new ArrayList<Student>();
    }

    //getAllStudent
    public ArrayList<Student> getAllStudent() {
        try {
            //buat obj koneksi
            Connection conn = DbConnection.getConnection();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT * FROM "+tablename);
            this.data.clear();
            while(rs.next()){
                data.add(new Student(
                        rs.getString("rNumber"),
                        rs.getString("sName"),
                        rs.getString("sbirthday"),
                        rs.getString("grade")
                ));
            }
            
        } catch (SQLException ex) {
            System.out.println("Gagal baca data");
            ex.printStackTrace();
        }finally{
            return this.data;
        }
    }

    //addStudent
    public void addStudent(Student newStudent) throws SQLException {
        
        String query = "INSERT INTO "+tablename
                  + " (rNumber, sName, sbirthday, grade, password)" 
                  + " values(?,?,?,?,?)";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, newStudent.getrNumber());
        ps.setString(2, newStudent.getsName());
        ps.setString(3, newStudent.getSbirthday());
        ps.setString(4, newStudent.getGrade());
        ps.setString(5, newStudent.getPassword());
        
        ps.execute();
        ps.close();
        
    }

    
//    public ArrayList<String> autoId() throws SQLException
//    {
//        try {
//            //buat obj koneksi
//            Connection conn = DbConnection.getConnection();
//            Statement stmnt = conn.createStatement();
//            ResultSet rs = stmnt.executeQuery("SELECT rNumber FROM "+tablename+ " ORDER BY rNumber DESC LIMIT 1");
//            this.autoId.clear();
//            if(rs.next()){
//                autoId.add(rs.getString("rNumber"));
//            }
//            
//        } catch (SQLException ex) {
//            System.out.println("Gagal baca data");
//            ex.printStackTrace();
//        }finally{
//            return this.autoId;
//        }
//    }
    
    //get last id
    public String getLastrNumber() {
        String result = null;
        if (data.size() > 0) {
            int jumlahData = data.size();
            //indeks 0 s.d. jumlahData-1
            int indexTerakhir = jumlahData - 1;
            Student terakhir = data.get(indexTerakhir);
            result = terakhir.getrNumber();
        }
        return result;
    }

    public void ubah (Student s) throws SQLException{
        String query = "UPDATE "+tablename+" set "
                + "sName=?,sbirthday=?,grade=?,password=? "
                + "where rNumber=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, s.getsName());
        ps.setString(2, s.getSbirthday());
        ps.setString(3, s.getGrade());
        ps.setString(4, s.getPassword());
        ps.setString(5, s.getrNumber());
        ps.executeUpdate();
        ps.close();
    }
    
    //hapus
    public boolean hapus(String rNumber) throws SQLException{
        boolean status;
        String query = "DELETE FROM "+tablename+" where rNumber=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, rNumber);
        status = ps.execute();
        ps.close();
        return status;
    }
    
    
    //get last id
    public String getPassword(String rNumber) {
        String result = null;
        
        try {
            //buat obj koneksi
            Connection conn = DbConnection.getConnection();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT password FROM `student` where rNumber = '"+rNumber+"'");
            while(rs.next()){
                result = rs.getString("password");
            }
            
        } catch (SQLException ex) {
            System.out.println("Gagal baca data");
            ex.printStackTrace();
        }finally{
            return result;
        }
    }
    //cari
    //filter
}
