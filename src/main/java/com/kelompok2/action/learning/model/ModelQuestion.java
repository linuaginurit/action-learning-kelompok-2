/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import com.kelompok2.action.learning.Result;
import com.kelompok2.action.learning.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author dokua
 */
public class ModelQuestion {

    ArrayList<Question> data;
    ArrayList<Mark> dataMark;
    private String tablename = "setPaper";

    public void setData(ArrayList<Question> data) {
        this.data = data;
    }

    public ArrayList<Question> getData() {
        return data;
    }

    public ModelQuestion() {
        data = new ArrayList<Question>();
    }
//    getall pegawai

    public ArrayList<Question> getAllQuestion() {
        try {
            //buat obj koneksi
        
            Connection conn = DbConnection.getConnection();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT * FROM " + tablename);
            this.data.clear();
            while (rs.next()) {
                data.add(new Question(
                        rs.getString("exam"),
                        rs.getString("subject"),
                        rs.getString("subcode"),
                        rs.getString("grade"),
                        rs.getInt("queNumber"),
                        rs.getString("que"),
                        rs.getString("ans01"),
                        rs.getString("ans02"),
                        rs.getString("ans03"),
                        rs.getString("ans04"),
                        rs.getString("correctans")
                ));

            }

//            return this.data;
        } catch (SQLException ex) {
            System.out.println("Gagal baca data");
            ex.printStackTrace();
        } finally {
            return this.data;
        }
    }

    //addQuestion
    public void addQuestion(Question newQuestion) throws SQLException {
        String query = "INSERT INTO "
                + tablename
                + "(`exam`, `subject`, `subcode`, `grade`,queNumber, `que`, `ans01`, `ans02`, `ans03`, `ans04`, `correctans`) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, newQuestion.getExam());
        ps.setString(2, newQuestion.getSubject());
        ps.setString(3, newQuestion.getSubcode());
        ps.setString(4, newQuestion.getGrade());
        ps.setInt(5, newQuestion.getQueNumber());
        ps.setString(6, newQuestion.getQue());
        ps.setString(7, newQuestion.getAns01());
        ps.setString(8, newQuestion.getAns02());
        ps.setString(9, newQuestion.getAns03());
        ps.setString(10, newQuestion.getAns04());
        ps.setString(11, newQuestion.getCorrectans());
        
     
        ps.execute();
        ps.close();
    }
    
    public void ubah (Question s) throws SQLException{
        String query = "UPDATE "+tablename+" set "
                + "exam=?,subject=?,subcode=?,grade=?,que=?,ans01=? ,ans02=? ,ans03=? ,ans04=? ,correctans=?  "
                + "where queNumber=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, s.getExam());
        ps.setString(2, s.getSubject());
        ps.setString(3, s.getSubcode());
        ps.setString(4, s.getGrade());
        ps.setString(5, s.getQue());
        ps.setString(6, s.getAns01());
        ps.setString(7, s.getAns02());
        ps.setString(8, s.getAns03());
        ps.setString(9, s.getAns04());
        ps.setString(10, s.getCorrectans());
        ps.setInt(11, s.getQueNumber());
        ps.executeUpdate();
        ps.close();
    }
    
    //hapus
    public boolean hapus(String quectionNumber) throws SQLException{
        boolean status;
        String query = "DELETE FROM "+tablename+" where queNumber=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, quectionNumber);
        status = ps.execute();
        ps.close();
        return status;
    }
    public ArrayList<Mark> getResult(String regnum, String subcode) {
        try {
            Connection conn = DbConnection.getConnection();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT `subcode`, `regnum`, `totalque`, `correctans`, `falseans`, `avgmark` FROM `mark` WHERE regnum="+regnum+" and subcode="+subcode);
            this.dataMark.clear();
            while (rs.next()) {
                dataMark.add(new Mark(
                        rs.getString("subcode"),
                        rs.getString("regnum"),
                        rs.getString("totalque"),
                        rs.getInt("correctans"),
                        rs.getInt("falseans"),
                        rs.getInt("avgmark")
                ));
            }
        } catch (SQLException ex) {
            System.out.println("Gagal baca data");
            ex.printStackTrace();
        } finally {
            return this.dataMark;
        }
    }
}
