/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import java.util.Objects;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class Student {
    String rNumber, sName, sbirthday, grade, password;

    public Student() {
    }

    public Student(String rNumber, String sName, String sbirthday, String grade, String password) {
        this.rNumber = rNumber;
        this.sName = sName;
        this.sbirthday = sbirthday;
        this.grade = grade;
        this.password = password;
    }

    Student(String rNumber, String sName, String sbirthday, String grade) {
        this.rNumber = rNumber;
        this.sName = sName;
        this.sbirthday = sbirthday;
        this.grade = grade;
    }

    Student(String rNumber, String grade, String password) {
        this.rNumber = rNumber;
        this.grade = grade;
        this.password = password;
    }

    public String getrNumber() {
        return rNumber;
    }

    public void setrNumber(String rNumber) {
        this.rNumber = rNumber;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getSbirthday() {
        return sbirthday;
    }

    public void setSbirthday(String sbirthday) {
        this.sbirthday = sbirthday;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Student{" + "rNumber=" + rNumber + ", sName=" + sName + ", sbirthday=" + sbirthday + ", grade=" + grade + ", password=" + password + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.rNumber);
        hash = 97 * hash + Objects.hashCode(this.sName);
        hash = 97 * hash + Objects.hashCode(this.sbirthday);
        hash = 97 * hash + Objects.hashCode(this.grade);
        hash = 97 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.rNumber, other.rNumber)) {
            return false;
        }
        if (!Objects.equals(this.sName, other.sName)) {
            return false;
        }
        if (!Objects.equals(this.sbirthday, other.sbirthday)) {
            return false;
        }
        if (!Objects.equals(this.grade, other.grade)) {
            return false;
        }
        return Objects.equals(this.password, other.password);
    }
    
    
}
