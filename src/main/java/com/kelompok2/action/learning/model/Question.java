/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import java.util.Objects;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class Question {
    String exam, subject, subcode, grade, que, ans01, ans02, ans03, ans04, correctans;
    int queNumber;

    public Question() {
    }

    public Question(String exam, String subject, String subcode, String grade, int queNumber, String que, String ans01, String ans02, String ans03, String ans04, String correctans) {
        this.exam = exam;
        this.subject = subject;
        this.subcode = subcode;
        this.grade = grade;
        this.queNumber = queNumber;
        this.que = que;
        this.ans01 = ans01;
        this.ans02 = ans02;
        this.ans03 = ans03;
        this.ans04 = ans04;
        this.correctans = correctans;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getQue() {
        return que;
    }

    public void setQue(String que) {
        this.que = que;
    }

    public String getAns01() {
        return ans01;
    }

    public void setAns01(String ans01) {
        this.ans01 = ans01;
    }

    public String getAns02() {
        return ans02;
    }

    public void setAns02(String ans02) {
        this.ans02 = ans02;
    }

    public String getAns03() {
        return ans03;
    }

    public void setAns03(String ans03) {
        this.ans03 = ans03;
    }

    public String getAns04() {
        return ans04;
    }

    public void setAns04(String ans04) {
        this.ans04 = ans04;
    }

    public String getCorrectans() {
        return correctans;
    }

    public void setCorrectans(String correctans) {
        this.correctans = correctans;
    }

    public int getQueNumber() {
        return queNumber;
    }

    public void setQueNumber(int queNumber) {
        this.queNumber = queNumber;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.exam);
        hash = 67 * hash + Objects.hashCode(this.subject);
        hash = 67 * hash + Objects.hashCode(this.subcode);
        hash = 67 * hash + Objects.hashCode(this.grade);
        hash = 67 * hash + Objects.hashCode(this.que);
        hash = 67 * hash + Objects.hashCode(this.ans01);
        hash = 67 * hash + Objects.hashCode(this.ans02);
        hash = 67 * hash + Objects.hashCode(this.ans03);
        hash = 67 * hash + Objects.hashCode(this.ans04);
        hash = 67 * hash + Objects.hashCode(this.correctans);
        hash = 67 * hash + this.queNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Question other = (Question) obj;
        if (this.queNumber != other.queNumber) {
            return false;
        }
        if (!Objects.equals(this.exam, other.exam)) {
            return false;
        }
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        if (!Objects.equals(this.subcode, other.subcode)) {
            return false;
        }
        if (!Objects.equals(this.grade, other.grade)) {
            return false;
        }
        if (!Objects.equals(this.que, other.que)) {
            return false;
        }
        if (!Objects.equals(this.ans01, other.ans01)) {
            return false;
        }
        if (!Objects.equals(this.ans02, other.ans02)) {
            return false;
        }
        if (!Objects.equals(this.ans03, other.ans03)) {
            return false;
        }
        if (!Objects.equals(this.ans04, other.ans04)) {
            return false;
        }
        return Objects.equals(this.correctans, other.correctans);
    }

    @Override
    public String toString() {
        return "Question{" + "exam=" + exam + ", subject=" + subject + ", subcode=" + subcode + ", grade=" + grade + ", que=" + que + ", ans01=" + ans01 + ", ans02=" + ans02 + ", ans03=" + ans03 + ", ans04=" + ans04 + ", correctans=" + correctans + ", queNumber=" + queNumber + '}';
    }

}
