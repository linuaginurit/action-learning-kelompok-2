/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.model;

import com.kelompok2.action.learning.ExamLogin;
import com.kelompok2.action.learning.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author dokua
 */
public class ModelExamLogin {

    ArrayList<ExamLogin> data;
    private String tablename = "setPaper";

    public void setData(ArrayList<ExamLogin> data) {
        this.data = data;
    }

    public ArrayList<ExamLogin> getData() {
        return data;
    }

    public ModelExamLogin() {
        data = new ArrayList<ExamLogin>();
    }
//    getall pegawai

    public ArrayList<ExamLogin> getAllExamLogin() {
        try {
            //buat obj koneksi
            Connection conn = DbConnection.getConnection();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT * FROM " + tablename);
            this.data.clear();
            while (rs.next()) {
                data.add(new ExamLogin(
                        rs.getInt("id"),
                        rs.getString("nama"),
                        rs.getString("email"),
                        rs.getString("jenis_kelamin"),
                        new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("tgl_lahir")),
                        rs.getString("departemen"),
                        rs.getString("alamat")));

            }

//            return this.data;
        } catch (SQLException ex) {
            System.out.println("Gagal baca data");
            ex.printStackTrace();
        } finally {
            return this.data;
        }
    }

    //addExamLogin
    public void distExamLogin(ExamLogin newExamLogin) throws SQLException {
//        if(data.contains(newExamLogin)){
//            throw new InputMismatchException("Email sudah terdaftar");
//        }
//        data.add(newExamLogin);

        String query = "SELECT DISTINCT `subcode` FROM `setpaper` WHERE grade=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, newExamLogin.getNama());
        ps.setString(2, newExamLogin.getEmail());
        ps.setString(3, newExamLogin.getJenisKelamin());
        ps.setString(4, newExamLogin.getTglLahir());
        ps.setString(5, newExamLogin.getDepartemen());
        ps.setString(6, newExamLogin.getAlamat());

        ps.execute();
        ps.close();
    }

    //getLastID
    public int getLastId() {
        if (data.size() > 0) {
            ExamLogin terakhir = data.get(data.size() - 1);
            return terakhir.getId();
        } else {
            return 0;
        }

    }

//ubah
    public void ubah(ExamLogin p) throws SQLException {
        String query = "UPDATE " + tablename + " set "
                + "nama=?,email=?,jenis_kelamin=?,tgl_lahir=?,departemen=?,alamat=?"
                + " where id=?";
        Connection conn = DbConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, p.getNama());
        ps.setString(2, p.getEmail());
        ps.setString(3, p.getJenisKelamin());
        ps.setString(4, p.getTglLahir());
        ps.setString(5, p.getDepartemen());
        ps.setString(6, p.getAlamat());
        ps.setInt(7, p.getId());

        ps.executeUpdate();
        ps.close();
    }

}
