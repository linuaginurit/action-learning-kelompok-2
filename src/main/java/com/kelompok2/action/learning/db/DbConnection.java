/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kelompok2.action.learning.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Agung Firdaus
 * @author Ahmad Rifai
 * @author Amalia
 * @author Ridho Rifhani
 * @author Saifin Nuha
 * 
 */
public class DbConnection {
    private static String url,user,pass;
    
    static void init(){
        FileInputStream in = null;
        try {
            //baca config
            in = new FileInputStream("db.properties");
            Properties props = new Properties();
            props.load(in);
            url = props.getProperty("url","jdbc:mysql://localhost:3306/onlineexam");
            user = props.getProperty("user","root");
            pass = props.getProperty("pass","");
        } catch (FileNotFoundException ex) {
            System.out.println("Your configuration file not found =>"+ ex);
        } catch (IOException ex) {
            System.out.println("Cannot read your configuration file =>"+ ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static Connection getConnection(){
        init();
        try {
            return DriverManager.getConnection(url,user,pass);
        } catch (SQLException ex) {
            System.out.println("Cannot create connection =>"+ ex);
            ex.printStackTrace();
            return null;
        }
    }
}
